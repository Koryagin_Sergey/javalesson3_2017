package com.sourceit;


import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Book book1 = new Book();
        Book book2 = new Book();
        Book book3 = new Book();

        book1.name = "Java. The Complete Reference.";
        book1.author = "Schildt";
        book1.year = 2012;

        book2.name = "Thinking in Java";
        book2.author = "Bruce Eckel";
        book2.year = 2009;

        book3.name = "Core Java";
        book3.author = "Horstmann";
        book3.year = 2007;


        Book[] arrBook = {book1, book2, book3};
        Book oldBook = new Book();
        for (int i = 0; i < arrBook.length; i++) {
            if (i == 0) {
                oldBook = arrBook[i];
            }
            if (oldBook.year > arrBook[i].year) {
                oldBook = arrBook[i];

            }
        }
        System.out.println(oldBook.author);

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name author: ");
        String nameAuthor = scan.nextLine();

        for (int i = 0; i < arrBook.length; i++) {
            if (nameAuthor.equals(arrBook[i].author)) {
                System.out.println(arrBook[i].name);
            }
        }
        System.out.println("Enter year: ");
        int yearRandom = Integer.parseInt(scan.nextLine());

        for (int i = 0; i < arrBook.length; i++) {
            if (yearRandom > arrBook[i].year) {
                System.out.println("Book title: " + arrBook[i].name + ". Author: " + arrBook[i].author + ". Year: " + arrBook[i].year + ".");
            }
        }

    }
}

class Book {
    String name;
    String author;
    int year;

}
